package com.PointOfSales.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Data
public class Login {
    @Id
    private String email;
    private String password;
    private LocalDate tanggalLogin;
    private String role;
    @Column(length = 1000)
    private String token;
}
