package com.PointOfSales.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Karyawan {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String nik;
    private String nama;
    private String alamat;
    private String jenisKelamin;
    @OneToOne
    private Login login;
}
