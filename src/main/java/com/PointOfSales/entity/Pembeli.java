package com.PointOfSales.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
public class Pembeli {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Integer id;
    private String namaPembeli;
    private String noTelepon;
    private String alamat;
    private String tanggalLahir;
    private String jenisKelamin;
    @OneToOne
    private Login login;
}
