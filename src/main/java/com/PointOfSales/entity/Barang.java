package com.PointOfSales.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;

@Entity
@Data
public class Barang {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Integer id;
    private String kodeBarang;
    private String namaBarang;
    private Integer stokBarang;
    private BigDecimal harga;
    private String detailBarang;
}
