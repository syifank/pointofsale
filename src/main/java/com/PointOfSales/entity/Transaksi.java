package com.PointOfSales.entity;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Data
public class Transaksi {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Integer id;
    private String kodeTransaksi;
    private LocalDateTime tanggalTransaksi;
    @ManyToOne
    private Barang barang;
    @ManyToOne
    private Pembeli pembeli;
    @ManyToOne
    private Karyawan karyawan;
    private Integer jumlahBeli;
    private BigDecimal totalBayar;
}
