package com.PointOfSales.dto.barang;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class BarangRequest {
    private String kodeBarang;
    private String namaBarang;
    private Integer stokBarang;
    private BigDecimal harga;
    private String detailBarang;
}
