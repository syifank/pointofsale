package com.PointOfSales.dto.karyawan;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class KaryawanRequest {
    private String nik;
    private String nama;
    private String alamat;
    private String jenisKelamin;
    private String email;
    private String password;
    private LocalDateTime tanggalLogin;
    private String role;
}
