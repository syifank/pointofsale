package com.PointOfSales.dto.karyawan;

import lombok.Data;

@Data
public class KaryawanResponse {
    private String nik;
    private String nama;
    private String alamat;
    private String jenisKelamin;
}
