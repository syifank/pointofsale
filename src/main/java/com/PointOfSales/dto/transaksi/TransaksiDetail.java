package com.PointOfSales.dto.transaksi;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
public class TransaksiDetail {
    private String namaPembeli;
    private String noTelepon;
    private String alamat;
    private List<TransaksiInfo> listTransaksi;
}
