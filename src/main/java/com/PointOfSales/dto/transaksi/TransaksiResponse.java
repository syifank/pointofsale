package com.PointOfSales.dto.transaksi;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

@Data
public class TransaksiResponse {
    private String kodeTransaksi;
    private LocalDateTime tanggalTransaksi;
    private String kodeBarang;
    private String namaBarang;
    private String namaPembeli;
    private String noTelepon;
    private String alamat;
    private BigDecimal hargaBarang;
    private Integer jumlahBeli;
    private BigDecimal totalBayar;
}
