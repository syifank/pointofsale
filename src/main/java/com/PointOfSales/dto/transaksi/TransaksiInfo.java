package com.PointOfSales.dto.transaksi;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class TransaksiInfo {
    private String kodeTransaksi;
    private String kodeBarang;
    private String namaBarang;
    private Integer jumlahBeli;
    private BigDecimal totalBayar;
    private LocalDateTime tanggalTransaksi;
}
