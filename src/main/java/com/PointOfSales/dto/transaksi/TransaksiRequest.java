package com.PointOfSales.dto.transaksi;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

@Data
public class TransaksiRequest {
    private String kodeTransaksi;
    //private LocalDateTime tanggalTransaksi;
    private Integer idBarang;
    private Integer idPembeli;
    private Integer idKaryawan;
    //private BigDecimal hargaBarang;
    private Integer jumlahBeli;
    //private BigDecimal totalBayar;
}
