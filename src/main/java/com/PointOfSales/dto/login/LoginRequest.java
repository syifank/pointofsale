package com.PointOfSales.dto.login;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class LoginRequest {
    private String email;
    private String password;
    private LocalDate tanggalLogin;
    private String role;
}
