package com.PointOfSales.dto.pembeli;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class PembeliRequest {
    private String namaPembeli;
    private String noTelepon;
    private String alamat;
    private String tanggalLahir;
    private String jenisKelamin;
    private String email;
    private String password;
    private LocalDateTime tanggalLogin;
    private String role;
}
