package com.PointOfSales.dto.pembeli;

import lombok.Data;

import java.util.Date;

@Data
public class PembeliResponse {
    private String namaPembeli;
    private String noTelepon;
    private String alamat;
    private String tanggalLahir;
    private String jenisKelamin;
}
