package com.PointOfSales.controller;

import com.PointOfSales.dto.karyawan.KaryawanRequest;
import com.PointOfSales.dto.karyawan.KaryawanResponse;
import com.PointOfSales.dto.login.LoginRequest;
import com.PointOfSales.entity.Karyawan;
import com.PointOfSales.service.KaryawanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/karyawan")
public class KaryawanController {

    @Autowired
    private KaryawanService karyawanService;

    @GetMapping("/all")
    public ResponseEntity<List<Karyawan>> getKaryawanAll(){
        List<Karyawan> response = karyawanService.getKaryawanAll();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Optional<Karyawan>> getKaryawanById (@RequestParam("id") Integer id){
        Optional<Karyawan> response = karyawanService.getById(id);
        return ResponseEntity.ok(response);
    }

    @PostMapping
    public ResponseEntity<KaryawanResponse> saveKaryawan(@RequestBody KaryawanRequest request){
        KaryawanResponse response = karyawanService.saveKaryawan(request);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<KaryawanResponse> updateKaryawan(@PathVariable("id") Integer id, KaryawanRequest request) throws Exception{
        KaryawanResponse response = karyawanService.updateKaryawan(id, request);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteById(@PathVariable("id") Integer id) throws Exception{
        String response = karyawanService.deleteKaryawanById(id);
        return ResponseEntity.ok(response);
    }
}
