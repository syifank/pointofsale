package com.PointOfSales.controller;

import com.PointOfSales.dto.transaksi.TransaksiDetail;
import com.PointOfSales.dto.transaksi.TransaksiRequest;
import com.PointOfSales.dto.transaksi.TransaksiResponse;
import com.PointOfSales.entity.Transaksi;
import com.PointOfSales.service.TransaksiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/transaksi")
public class TransaksiController {

    @Autowired
    private TransaksiService transaksiService;

    @GetMapping("/all")
    public ResponseEntity<List<Transaksi>> getTransaksiAll(@RequestHeader("token") String token){
        List<Transaksi> response = transaksiService.getAllTransaksi(token);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Optional<Transaksi>> getTransaksiById (@RequestHeader("token") String token, @RequestParam("id") Integer id){
        Optional<Transaksi> response = transaksiService.getTransaksiById(token, id);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{id}/detail-transaksi")
    public ResponseEntity<TransaksiDetail> getTransaksiDetail(@RequestHeader("token") String token, @PathVariable("id") Integer idPembeli){
        TransaksiDetail response = transaksiService.getTransaksiByIdPembeli(token, idPembeli);
        return ResponseEntity.ok(response);
    }

    @PostMapping
    public ResponseEntity<TransaksiResponse> saveTransaksi(@RequestHeader("token") String token, @RequestBody TransaksiRequest request){
        TransaksiResponse response = transaksiService.saveTransaksi(token, request);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<TransaksiResponse> updateTransaksi(@RequestHeader("token") String token, @PathVariable("id") Integer id, TransaksiRequest request) throws Exception{
        TransaksiResponse response = transaksiService.updateTransaksi(token, id, request);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteById(@RequestHeader("token") String token, @PathVariable("id") Integer id) throws Exception{
        String response = transaksiService.deleteTransaksiById(token, id);
        return ResponseEntity.ok(response);
    }
}
