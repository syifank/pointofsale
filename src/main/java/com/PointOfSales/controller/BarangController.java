package com.PointOfSales.controller;

import com.PointOfSales.dto.barang.BarangRequest;
import com.PointOfSales.dto.barang.BarangResponse;
import com.PointOfSales.entity.Barang;
import com.PointOfSales.service.BarangService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/barang")
public class BarangController {

    @Autowired
    private BarangService barangService;

    @GetMapping("/all")
    public ResponseEntity<List<Barang>> getBarangAll(){
        List<Barang> response = barangService.getBarangAll();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Optional<Barang>> getBarangById (@RequestParam("id") Integer id){
        Optional<Barang> response = barangService.getById(id);
        return ResponseEntity.ok(response);
    }

    @PostMapping
    public ResponseEntity<BarangResponse> saveBarang(@RequestBody BarangRequest request){
        BarangResponse response = barangService.saveBarang(request);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<BarangResponse> updateBarang(@PathVariable("id") Integer id, BarangRequest request) throws Exception{
        BarangResponse response = barangService.updateBarang(id, request);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteById(@PathVariable("id") Integer id) throws Exception{
        String response = barangService.deleteBarangById(id);
        return ResponseEntity.ok(response);
    }
}
