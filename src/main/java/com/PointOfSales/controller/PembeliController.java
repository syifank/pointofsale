package com.PointOfSales.controller;

import com.PointOfSales.dto.pembeli.PembeliRequest;
import com.PointOfSales.dto.pembeli.PembeliResponse;
import com.PointOfSales.entity.Pembeli;
import com.PointOfSales.service.PembeliService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/pembeli")
public class PembeliController {

    @Autowired
    private PembeliService pembeliService;

    @GetMapping("/all")
    public ResponseEntity<List<Pembeli>> getPembeliAll(){
        List<Pembeli> response = pembeliService.getPembeliAll();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Optional<Pembeli>> getPembeliById (@RequestParam("id") Integer id){
        Optional<Pembeli> response = pembeliService.getById(id);
        return ResponseEntity.ok(response);
    }

    @PostMapping
    public ResponseEntity<PembeliResponse> savePembeli(@RequestBody PembeliRequest request){
        PembeliResponse response = pembeliService.savePembeli(request);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<PembeliResponse> updatePembeli(@PathVariable("id") Integer id, PembeliRequest request) throws Exception{
        PembeliResponse response = pembeliService.updatePembeli(id, request);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteById(@PathVariable("id") Integer id) throws Exception{
        String response = pembeliService.deletePembeliById(id);
        return ResponseEntity.ok(response);
    }
}
