package com.PointOfSales.service;

import com.PointOfSales.dto.karyawan.KaryawanRequest;
import com.PointOfSales.dto.karyawan.KaryawanResponse;
import com.PointOfSales.dto.login.LoginRequest;
import com.PointOfSales.entity.Karyawan;
import com.PointOfSales.entity.Login;
import com.PointOfSales.repository.KaryawanRepository;
import com.PointOfSales.repository.LoginRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class KaryawanService {

    @Autowired
    private KaryawanRepository karyawanRepository;

    @Autowired
    private LoginRepository loginRepository;

    public KaryawanResponse saveKaryawan(KaryawanRequest request){
        KaryawanResponse response = new KaryawanResponse();
        Login login = new Login();
        Karyawan karyawan = new Karyawan();

        karyawan.setNik(request.getNik());
        karyawan.setNama(request.getNama());
        karyawan.setAlamat(request.getAlamat());
        karyawan.setJenisKelamin(request.getJenisKelamin());
        karyawanRepository.save(karyawan);

        login.setEmail(request.getEmail());
        login.setPassword(request.getPassword());
        login.setRole(request.getRole());
        loginRepository.save(login);

        response.setNik(karyawan.getNik());
        response.setNama(karyawan.getNama());
        response.setAlamat(karyawan.getAlamat());
        response.setJenisKelamin(karyawan.getJenisKelamin());
        return response;
    }

    public List<Karyawan> getKaryawanAll(){
        List<Karyawan> karyawanList = null;
        karyawanList = karyawanRepository.findAll();
        return karyawanList;
    }

    public Optional<Karyawan> getById(Integer id) {
        Optional<Karyawan> karyawan = karyawanRepository.findById(id);
        return karyawan;
    }

    public KaryawanResponse updateKaryawan(Integer id, KaryawanRequest request) throws Exception {
        Optional<Karyawan> karyawan = karyawanRepository.findById(id);
        Karyawan k = new Karyawan();
        if (!karyawan.get().getId().equals(id)){
            throw new Exception();
        } else {
            k.setNik(request.getNik());
            k.setNama(request.getNama());
            k.setAlamat(request.getAlamat());
            k.setJenisKelamin(request.getJenisKelamin());
            karyawanRepository.save(k);
        }
        KaryawanResponse response = new KaryawanResponse();
        response.setNik(request.getNik());
        response.setNama(request.getNama());
        response.setAlamat(request.getAlamat());
        response.setJenisKelamin(request.getJenisKelamin());
        return response;
    }

    public String deleteKaryawanById(Integer id) throws Exception{
        String response = "";
        Optional<Karyawan> karyawan = null;
        try{
            karyawan = karyawanRepository.findById(id);
            karyawanRepository.deleteById(id);
            response = "Data Karyawan berhasil dihapus";
        } catch (Exception e){
            throw new Exception();
        }
        return response;
    }
}
