package com.PointOfSales.service;

import com.PointOfSales.dto.pembeli.PembeliRequest;
import com.PointOfSales.dto.pembeli.PembeliResponse;
import com.PointOfSales.entity.Login;
import com.PointOfSales.entity.Pembeli;
import com.PointOfSales.repository.LoginRepository;
import com.PointOfSales.repository.PembeliRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PembeliService {

    @Autowired
    private PembeliRepository pembeliRepository;

    @Autowired
    private LoginRepository loginRepository;

    public PembeliResponse savePembeli(PembeliRequest request){
        PembeliResponse response = new PembeliResponse();
        Pembeli pembeli = new Pembeli();
        Login login = new Login();

        pembeli.setNamaPembeli(request.getNamaPembeli());
        pembeli.setNoTelepon(request.getNoTelepon());
        pembeli.setAlamat(request.getAlamat());
        pembeli.setTanggalLahir(request.getTanggalLahir());
        pembeli.setJenisKelamin(request.getJenisKelamin());
        pembeliRepository.save(pembeli);

        login.setEmail(request.getEmail());
        login.setPassword(request.getPassword());
        login.setRole(request.getRole());
        loginRepository.save(login);

        response.setNamaPembeli(pembeli.getNamaPembeli());
        response.setNoTelepon(pembeli.getNoTelepon());
        response.setAlamat(pembeli.getAlamat());
        response.setTanggalLahir(pembeli.getTanggalLahir());
        response.setJenisKelamin(pembeli.getJenisKelamin());
        return response;
    }

    public List<Pembeli> getPembeliAll(){
        List<Pembeli> pembeliList = null;
        pembeliList = pembeliRepository.findAll();
        return pembeliList;
    }

    public Optional<Pembeli> getById(Integer id) {
        Optional<Pembeli> pembeli = pembeliRepository.findById(id);
        return pembeli;
    }

    public PembeliResponse updatePembeli(Integer id, PembeliRequest request) throws Exception {
        Optional<Pembeli> pembeli = pembeliRepository.findById(id);
        Pembeli p = new Pembeli();
        if (!pembeli.get().getId().equals(id)){
            throw new Exception();
        } else {
            p.setNamaPembeli(request.getNamaPembeli());
            p.setNoTelepon(request.getNoTelepon());
            p.setAlamat(request.getAlamat());
            p.setTanggalLahir(request.getTanggalLahir());
            p.setJenisKelamin(request.getJenisKelamin());
            pembeliRepository.save(p);
        }
        PembeliResponse response = new PembeliResponse();
        response.setNamaPembeli(request.getNamaPembeli());
        response.setNoTelepon(request.getNoTelepon());
        response.setAlamat(request.getAlamat());
        response.setTanggalLahir(request.getTanggalLahir());
        response.setJenisKelamin(request.getJenisKelamin());
        return response;
    }

    public String deletePembeliById(Integer id) throws Exception{
        String response = "";
        Optional<Pembeli> pembeli = null;
        try{
            pembeli = pembeliRepository.findById(id);
            pembeliRepository.deleteById(id);
            response = "Data Pembeli berhasil dihapus";
        } catch (Exception e){
            throw new Exception();
        }
        return response;
    }
}
