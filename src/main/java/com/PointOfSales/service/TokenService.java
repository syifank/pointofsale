package com.PointOfSales.service;

import com.PointOfSales.entity.Login;
import com.PointOfSales.repository.KaryawanRepository;
import com.PointOfSales.repository.LoginRepository;
import com.PointOfSales.repository.PembeliRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TokenService {

    @Autowired
    private LoginRepository loginRepository;

    @Autowired
    private PembeliRepository pembeliRepository;

    @Autowired
    private KaryawanRepository karyawanRepository;

    public boolean getToken (String token){
        Optional<Login> login = loginRepository.findByToken(token);
        boolean validate = login.isPresent() ? true : false;
        return validate;
    }
}
