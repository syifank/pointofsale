package com.PointOfSales.service;

import com.PointOfSales.dto.barang.BarangRequest;
import com.PointOfSales.dto.barang.BarangResponse;
import com.PointOfSales.entity.Barang;
import com.PointOfSales.entity.Karyawan;
import com.PointOfSales.repository.BarangRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BarangService {

    @Autowired
    private BarangRepository barangRepository;

    public BarangResponse saveBarang(BarangRequest request){
        BarangResponse response = new BarangResponse();
        Barang barang = new Barang();
        barang.setKodeBarang(request.getKodeBarang());
        barang.setNamaBarang(request.getNamaBarang());
        barang.setStokBarang(request.getStokBarang());
        barang.setHarga(request.getHarga());
        barang.setDetailBarang(request.getDetailBarang());
        barangRepository.save(barang);

        response.setKodeBarang(barang.getKodeBarang());
        response.setNamaBarang(barang.getNamaBarang());
        response.setStokBarang(barang.getStokBarang());
        response.setHarga(barang.getHarga());
        response.setDetailBarang(barang.getDetailBarang());
        return response;
    }

    public List<Barang> getBarangAll(){
        List<Barang> barangList = null;
        barangList = barangRepository.findAll();
        return barangList;
    }

    public Optional<Barang> getById(Integer id) {
        Optional<Barang> barang = barangRepository.findById(id);
        return barang;
    }

    public BarangResponse updateBarang(Integer id, BarangRequest request) throws Exception {
        Optional<Barang> barang = barangRepository.findById(id);
        Barang b = new Barang();
        if (!barang.get().getId().equals(id)){
            throw new Exception();
        } else {
            b.setKodeBarang(request.getKodeBarang());
            b.setNamaBarang(request.getNamaBarang());
            b.setStokBarang(request.getStokBarang());
            b.setHarga(request.getHarga());
            b.setDetailBarang(request.getDetailBarang());
            barangRepository.save(b);
        }
        BarangResponse response = new BarangResponse();
        response.setKodeBarang(request.getKodeBarang());
        response.setNamaBarang(request.getNamaBarang());
        response.setStokBarang(request.getStokBarang());
        response.setHarga(request.getHarga());
        response.setDetailBarang(request.getDetailBarang());
        return response;
    }

    public String deleteBarangById(Integer id) throws Exception{
        String response = "";
        Optional<Barang> barang = null;
        try{
            barang = barangRepository.findById(id);
            barangRepository.deleteById(id);
            response = "Data Barang berhasil dihapus";
        } catch (Exception e){
            throw new Exception();
        }
        return response;
    }
}
