package com.PointOfSales.service;

import com.PointOfSales.dto.login.LoginRequest;
import com.PointOfSales.dto.login.LoginResponse;
import com.PointOfSales.entity.Login;
import com.PointOfSales.repository.LoginRepository;
import com.PointOfSales.util.JwtToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class LoginService {

    @Autowired
    private LoginRepository loginRepository;

    public LoginResponse login(LoginRequest request){
        Optional<Login> login = loginRepository.findByEmailAndPassword(request.getEmail(), request.getPassword());
        Login l = new Login();
        if (login.isPresent()){
            l.setEmail(login.get().getEmail());
            l.setPassword(login.get().getPassword());
            l.setRole(login.get().getRole());
            l.setTanggalLogin(LocalDate.now());
            l.setToken(JwtToken.getToken(request));
            loginRepository.save(l);
        }
        LoginResponse response = new LoginResponse();
        response.setToken(l.getToken());
        return response;
    }
}
