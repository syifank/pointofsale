package com.PointOfSales.service;

import com.PointOfSales.dto.transaksi.TransaksiDetail;
import com.PointOfSales.dto.transaksi.TransaksiInfo;
import com.PointOfSales.dto.transaksi.TransaksiRequest;
import com.PointOfSales.dto.transaksi.TransaksiResponse;
import com.PointOfSales.entity.Barang;
import com.PointOfSales.entity.Karyawan;
import com.PointOfSales.entity.Pembeli;
import com.PointOfSales.entity.Transaksi;
import com.PointOfSales.repository.BarangRepository;
import com.PointOfSales.repository.KaryawanRepository;
import com.PointOfSales.repository.PembeliRepository;
import com.PointOfSales.repository.TransaksiRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TransaksiService {

    @Autowired
    private TransaksiRepository transaksiRepository;

    @Autowired
    private BarangRepository barangRepository;

    @Autowired
    private PembeliRepository pembeliRepository;

    @Autowired
    private KaryawanRepository karyawanRepository;

    @Autowired
    private TokenService tokenService;

    public List<Transaksi> getAllTransaksi(String token){
        if (!tokenService.getToken(token)){
            return null;
        }
        List<Transaksi> listTransaksi = new ArrayList<>();
        for (Transaksi transaksi : transaksiRepository.findAll()) {
            listTransaksi.add(transaksi);
        }
        return listTransaksi;
    }

    public Optional<Transaksi> getTransaksiById(String token, Integer id){
        if (!tokenService.getToken(token)){
            return null;
        }
        Optional<Transaksi> transaksi = transaksiRepository.findById(id);
        return transaksi;
    }

    public TransaksiDetail getTransaksiByIdPembeli(String token, Integer idPembeli){
        if (!tokenService.getToken(token)){
            return null;
        }
        Optional<Pembeli> pembeli = pembeliRepository.findById(idPembeli);
        if (!pembeli.isPresent()){
            return null;
        }
        Pembeli p = pembeli.get();
        List<TransaksiInfo> listTransaksi = transaksiRepository.findDetailTransaksi(idPembeli);
        return TransaksiDetail.builder()
                .namaPembeli(pembeli.get().getNamaPembeli())
                .noTelepon(pembeli.get().getNoTelepon())
                .alamat(pembeli.get().getAlamat())
                .listTransaksi(listTransaksi)
                .build();
    }

    public TransaksiResponse saveTransaksi(String token, TransaksiRequest request){
        TransaksiResponse response = new TransaksiResponse();
        if (tokenService.getToken(token) == true){
            Optional<Pembeli> pembeli = pembeliRepository.findById(request.getIdPembeli());
            Optional<Karyawan> karyawan  = karyawanRepository.findById(request.getIdKaryawan());
            Optional<Barang> barang = barangRepository.findById(request.getIdBarang());

            Transaksi transaksi = new Transaksi();
            transaksi.setKodeTransaksi(request.getKodeTransaksi());
            transaksi.setPembeli(pembeli.get());
            transaksi.setKaryawan(karyawan.get());
            transaksi.setBarang(barang.get());
            transaksi.setJumlahBeli(request.getJumlahBeli());
            transaksi.setTotalBayar(barang.get().getHarga().multiply(new BigDecimal(request.getJumlahBeli())));
            transaksi.setTanggalTransaksi(LocalDateTime.now());
            transaksiRepository.save(transaksi);

            response.setKodeTransaksi(transaksi.getKodeTransaksi());
            response.setTanggalTransaksi(transaksi.getTanggalTransaksi());
            response.setKodeBarang(transaksi.getBarang().getKodeBarang());
            response.setNamaBarang(transaksi.getBarang().getNamaBarang());
            response.setNamaPembeli(transaksi.getPembeli().getNamaPembeli());
            response.setAlamat(transaksi.getPembeli().getAlamat());
            response.setNoTelepon(transaksi.getPembeli().getNoTelepon());
            response.setHargaBarang(transaksi.getBarang().getHarga());
            response.setTotalBayar(transaksi.getTotalBayar());
        } else {
            throw new RuntimeException();
        }
        return response;
    }

    public TransaksiResponse updateTransaksi(String token, Integer id, TransaksiRequest request){
        TransaksiResponse response = new TransaksiResponse();
        if (!tokenService.getToken(token)){
            return null;
        }
        Optional<Transaksi> t = transaksiRepository.findById(id);
        Optional<Pembeli> pembeli = pembeliRepository.findById(request.getIdPembeli());
        Optional<Karyawan> karyawan  = karyawanRepository.findById(request.getIdKaryawan());
        Optional<Barang> barang = barangRepository.findById(request.getIdBarang());

        Transaksi transaksi = t.get();
        transaksi.setKodeTransaksi(request.getKodeTransaksi());
        transaksi.setPembeli(pembeli.get());
        transaksi.setKaryawan(karyawan.get());
        transaksi.setBarang(barang.get());
        transaksi.setJumlahBeli(request.getJumlahBeli());
        transaksi.setTotalBayar(barang.get().getHarga().multiply(new BigDecimal(request.getJumlahBeli())));
        transaksi.setTanggalTransaksi(LocalDateTime.now());
        transaksiRepository.save(transaksi);

        response.setKodeTransaksi(transaksi.getKodeTransaksi());
        response.setTanggalTransaksi(transaksi.getTanggalTransaksi());
        response.setKodeBarang(transaksi.getBarang().getKodeBarang());
        response.setNamaBarang(transaksi.getBarang().getNamaBarang());
        response.setNamaPembeli(transaksi.getPembeli().getNamaPembeli());
        response.setAlamat(transaksi.getPembeli().getAlamat());
        response.setNoTelepon(transaksi.getPembeli().getNoTelepon());
        response.setHargaBarang(transaksi.getBarang().getHarga());
        response.setTotalBayar(transaksi.getTotalBayar());
        return response;
    }

    public String deleteTransaksiById(String token, Integer id){
        if (!tokenService.getToken(token)){
            return null;
        }
        Optional<Transaksi> t = transaksiRepository.findById(id);
        if (!t.isPresent()){
            return null;
        }
        transaksiRepository.delete(t.get());
        return String.format("Data berhasil dihapus dengan ID ", id);
    }
}
