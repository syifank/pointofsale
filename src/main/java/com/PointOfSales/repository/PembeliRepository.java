package com.PointOfSales.repository;

import com.PointOfSales.entity.Pembeli;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PembeliRepository extends JpaRepository<Pembeli, Integer> {
}
