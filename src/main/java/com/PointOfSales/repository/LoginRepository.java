package com.PointOfSales.repository;

import com.PointOfSales.entity.Login;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface LoginRepository extends JpaRepository<Login, Integer> {
    Optional<Login> findByEmailAndPassword(String email, String password);


    Optional<Login> findByToken(String token);
}
