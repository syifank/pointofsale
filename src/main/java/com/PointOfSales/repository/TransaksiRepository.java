package com.PointOfSales.repository;

import com.PointOfSales.dto.transaksi.TransaksiInfo;
import com.PointOfSales.entity.Transaksi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TransaksiRepository extends JpaRepository<Transaksi, Integer> {
    @Query("SELECT new com.PointOfSales.dto.transaksi.TransaksiInfo (t.kodeTransaksi, b.kodeBarang, b.namaBarang, t.jumlahBeli, " +
            "t.totalBayar, t.tanggalTransaksi) FROM Transaksi t JOIN t.barang b JOIN t.pembeli p WHERE p.id=:id")
    List<TransaksiInfo> findDetailTransaksi(@Param("id") Integer id);
}
