package com.PointOfSales.repository;

import com.PointOfSales.entity.Barang;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BarangRepository extends JpaRepository<Barang, Integer> {
}
