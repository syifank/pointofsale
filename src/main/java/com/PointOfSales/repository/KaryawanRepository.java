package com.PointOfSales.repository;

import com.PointOfSales.entity.Karyawan;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KaryawanRepository extends JpaRepository<Karyawan, Integer> {
}
