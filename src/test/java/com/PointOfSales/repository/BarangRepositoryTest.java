package com.PointOfSales.repository;


import com.PointOfSales.PointOfSalesApplication;
import com.PointOfSales.entity.Barang;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@SpringBootTest(classes = PointOfSalesApplication.class)
public class BarangRepositoryTest {

    @Autowired
    private BarangRepository barangRepository;

    private Barang getBarang(){
        Barang barang = new Barang();
        barang.setId(1);
        barang.setKodeBarang("12345");
        barang.setNamaBarang("Kipas Angin");
        barang.setHarga(BigDecimal.valueOf(100000));
        barang.setStokBarang(10);
        barang.setDetailBarang("Kipas Duduk");
        return barang;
    }

    @Test
    public void testSaveBarangPositiveCase(){
        Barang barang = getBarang();
        barangRepository.save(barang);
        Barang b = barangRepository.findById(getBarang().getId()).get();
        Assertions.assertEquals(barang.getId(), b.getId());
        Assertions.assertEquals(barang.getKodeBarang(), b.getKodeBarang());
        Assertions.assertEquals(barang.getNamaBarang(), b.getNamaBarang());
        Assertions.assertEquals(barang.getHarga(), BigDecimal.valueOf(100000));
        Assertions.assertEquals(barang.getStokBarang(), b.getStokBarang());
        Assertions.assertEquals(barang.getDetailBarang(), b.getDetailBarang());
    }

    @Test
    public void testSaveBarangNegativeCase(){
        Barang barang = getBarang();
        barangRepository.save(barang);
        Barang b = barangRepository.findById(getBarang().getId()).get();
        Assertions.assertNotEquals(3, b.getId());
        Assertions.assertNotEquals("barang.getKodeBarang()", b.getKodeBarang());
        Assertions.assertNotEquals("barang.getNamaBarang()", b.getNamaBarang());
        Assertions.assertNotEquals(1000.00, b.getHarga());
        Assertions.assertNotEquals(5, b.getStokBarang());
        Assertions.assertNotEquals("barang.getDetailBarang()", b.getDetailBarang());
    }

    @Test
    public void testFindAllBarangPositiveCase(){
        Barang barang = getBarang();
        barangRepository.save(barang);
        List<Barang> barangList = barangRepository.findAll();
        Assertions.assertEquals(1, barangList.size());
    }

    @Test
    public void testFindAllBarangNegativeCase(){
        Barang barang = getBarang();
        barangRepository.save(barang);
        List<Barang> barangList = barangRepository.findAll();
        Assertions.assertNotEquals(2, barangList.size());
    }

    @Test
    public void testUpdateBarangPositiveCase(){
        Barang barang = getBarang();
        barangRepository.save(barang);

        barang.setNamaBarang("Kipas Angin Modern");
        barang.setKodeBarang("78910");
        barang.setStokBarang(30);
        barangRepository.save(barang);

        Optional<Barang> b = barangRepository.findById(barangRepository.save(barangRepository.save(barang)).getId());
        Assertions.assertTrue(b.isPresent());
        Assertions.assertEquals("Kipas Angin Modern", b.get().getNamaBarang());
        Assertions.assertEquals("78910", b.get().getKodeBarang());
        Assertions.assertEquals(30, b.get().getStokBarang());
    }

    @Test
    public void testUpdateBarangNegativeCase(){
        Barang barang = getBarang();
        barangRepository.save(barang);

        barang.setNamaBarang("Kipas Angin Modern");
        barang.setKodeBarang("78910");
        barang.setStokBarang(30);
        barangRepository.save(barang);

        Optional<Barang> b = barangRepository.findById(barangRepository.save(barangRepository.save(barang)).getId());
        Assertions.assertTrue(b.isPresent());
        Assertions.assertNotEquals("Kipas Angin Jadu", b.get().getNamaBarang());
        Assertions.assertNotEquals("7891011", b.get().getKodeBarang());
        Assertions.assertNotEquals(20, b.get().getStokBarang());
    }

    @Test
    public void testDeleteBarangPositiveCase(){
        Barang barang = getBarang();
        Barang saveBarang = barangRepository.save(barang);

        barangRepository.delete(barang);
        Optional<Barang> b = barangRepository.findById(saveBarang.getId());

        Assertions.assertFalse(b.isPresent());
    }

    @Test
    public void testDeleteBarangNegativeCase(){
        Barang barang = getBarang();

        barangRepository.delete(barang);
        Optional<Barang> b = barangRepository.findById(barang.getId());

        Assertions.assertFalse(b.isPresent());
    }
}
