package com.PointOfSales.repository;

import com.PointOfSales.PointOfSalesApplication;
import com.PointOfSales.entity.Karyawan;
import com.PointOfSales.entity.Login;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@SpringBootTest(classes = PointOfSalesApplication.class)
public class LoginRepositoryTest {
    @Autowired
    private LoginRepository loginRepository;

    private Login getLogin(){
        Login login = new Login();
        login.setEmail("syifa@gmail.com");
        login.setPassword("123456");
        login.setTanggalLogin(LocalDate.now());
        login.setRole("Manusia");
        login.setToken("123456789qwertyuiop");
        return login;
    }

    @Test
    public void testSaveLoginPositiveCase(){
        Login login = getLogin();
        loginRepository.save(login);
        Optional<Login> l = loginRepository.findByEmailAndPassword(loginRepository.save(loginRepository.save(login)).getEmail(),
                loginRepository.save(loginRepository.save(login)).getPassword());
        Assertions.assertEquals(login.getEmail(), l.get().getEmail());
        Assertions.assertEquals(login.getPassword(), l.get().getPassword());
        Assertions.assertEquals(login.getTanggalLogin(), l.get().getTanggalLogin());
        Assertions.assertEquals(login.getRole(), l.get().getRole());
        Assertions.assertEquals(login.getToken(), l.get().getToken());
        loginRepository.delete(loginRepository.save(login));
    }

    @Test
    public void testSaveLoginNegativeCase(){
        Login login = getLogin();
        loginRepository.save(login);
        Optional<Login> l = loginRepository.findByEmailAndPassword(loginRepository.save(loginRepository.save(login)).getEmail(),
                loginRepository.save(loginRepository.save(login)).getPassword());
        Assertions.assertNotEquals("nabila@gmail.com", l.get().getEmail());
        Assertions.assertNotEquals("qwertyuiop", l.get().getPassword());
        Assertions.assertNotEquals(LocalDate.now().minusDays(1), l.get().getTanggalLogin());
        Assertions.assertNotEquals("Manusia Super", l.get().getRole());
        Assertions.assertNotEquals("qwertyuiop", l.get().getToken());
        loginRepository.delete(loginRepository.save(login));
    }

    @Test
    public void testFindAllLoginPositiveCase(){
        Login login = getLogin();
        loginRepository.save(login);
        List<Login> loginList = loginRepository.findAll();
        Assertions.assertEquals(1, loginList.size());
        loginRepository.delete(loginRepository.save(login));
    }

    @Test
    public void testFindAllLoginNegativeCase(){
        Login login = getLogin();
        loginRepository.save(login);
        List<Login> loginList = loginRepository.findAll();
        Assertions.assertNotEquals(5, loginList.size());
        loginRepository.delete(loginRepository.save(login));
    }

    @Test
    public void testUpdateLoginPositiveCase(){
        Login login = getLogin();
        loginRepository.save(login);

        login.setEmail("nabila1234@gmail.com");
        login.setPassword("qwerty");
        login.setTanggalLogin(LocalDate.now().minusDays(3));
        login.setRole("Manusia Biasa");
        login.setToken("akukuatakubisa");
        loginRepository.save(login);

        Optional<Login> l = loginRepository.findByEmailAndPassword(loginRepository.save(loginRepository.save(login)).getEmail(),
                loginRepository.save(loginRepository.save(login)).getPassword());
        Assertions.assertTrue(l.isPresent());
        Assertions.assertEquals("nabila1234@gmail.com", l.get().getEmail());
        Assertions.assertEquals("qwerty", l.get().getPassword());
        Assertions.assertEquals(LocalDate.now().minusDays(3), l.get().getTanggalLogin());
        Assertions.assertEquals("Manusia Biasa", l.get().getRole());
        Assertions.assertEquals("akukuatakubisa", l.get().getToken());
        loginRepository.delete(loginRepository.save(login));
    }

    @Test
    public void testUpdateLoginNegativeCase(){
        Login login = getLogin();
        loginRepository.save(login);

        login.setEmail("nabila1234@gmail.com");
        login.setPassword("qwerty");
        login.setTanggalLogin(LocalDate.now().minusDays(3));
        login.setRole("Manusia Biasa");
        login.setToken("akukuatakubisa");
        loginRepository.save(login);

        Optional<Login> l = loginRepository.findByEmailAndPassword(loginRepository.save(loginRepository.save(login)).getEmail(),
                loginRepository.save(loginRepository.save(login)).getPassword());
        Assertions.assertTrue(l.isPresent());
        Assertions.assertNotEquals("nnnn1234@gmail.com", l.get().getEmail());
        Assertions.assertNotEquals("qwertyuiop", l.get().getPassword());
        Assertions.assertNotEquals(LocalDate.now().minusDays(5), l.get().getTanggalLogin());
        Assertions.assertNotEquals("Manusiaa", l.get().getRole());
        Assertions.assertNotEquals("akukuatakubisahadapi", l.get().getToken());
        loginRepository.delete(loginRepository.save(login));
    }

    @Test
    public void testDeleteLoginPositiveCase(){
        Login login = getLogin();
        loginRepository.save(login);

        loginRepository.delete(login);
        Optional<Login> l = loginRepository.findByEmailAndPassword(login.getEmail(), login.getPassword());

        Assertions.assertFalse(l.isPresent());
        loginRepository.delete(loginRepository.save(login));
    }

    @Test
    public void testDeleteLoginNegativeCase(){
        Login login = getLogin();

        loginRepository.delete(login);
        Optional<Login> l = loginRepository.findByEmailAndPassword(login.getEmail(), login.getPassword());

        Assertions.assertFalse(l.isPresent());
    }
}
