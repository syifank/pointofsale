package com.PointOfSales.repository;

import com.PointOfSales.PointOfSalesApplication;
import com.PointOfSales.entity.Transaksi;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;


@SpringBootTest(classes = PointOfSalesApplication.class)
public class TransaksiRepositoryTest {

    @Autowired
    private TransaksiRepository transaksiRepository;

    private Transaksi getTransaksi(){
        Transaksi transaksi = new Transaksi();
        transaksi.setId(1);
        transaksi.setKodeTransaksi("TR12345");
        return transaksi;
    }

//    @Test
//    public void testSaveTransaksiPositiveCase(){
//        Transaksi transaksi = getTransaksi();
//        transaksiRepository.save(transaksi);
//        Optional<Transaksi> t = transaksiRepository.findById(transaksiRepository.save(transaksiRepository.save(transaksi)).getId());
//        Assertions.assertEquals(transaksi.getId(), t.get().getId());
//        Assertions.assertEquals(transaksi.getKodeTransaksi(), t.get().getKodeTransaksi());
//        transaksiRepository.delete(transaksiRepository.save(transaksi));
//    }

    @Test
    public void testSaveTransaksiNegativeCase(){
        Transaksi transaksi = getTransaksi();
        transaksiRepository.save(transaksi);
        Optional<Transaksi> t = transaksiRepository.findById(transaksiRepository.save(transaksiRepository.save(transaksi)).getId());
        Assertions.assertNotEquals(2, t.get().getId());
        Assertions.assertNotEquals("TR889900", t.get().getKodeTransaksi());
        transaksiRepository.delete(transaksiRepository.save(transaksi));
    }

//    @Test
//    public void testFindAllTransaksiPositiveCase(){
//        Transaksi transaksi = getTransaksi();
//        transaksiRepository.save(transaksi);
//        List<Transaksi> transaksiList = transaksiRepository.findAll();
//        Assertions.assertEquals(1, transaksiList.size());
//        transaksiRepository.delete(transaksiRepository.save(transaksi));
//    }

    @Test
    public void testFindAllTransaksiNegativeCase(){
        Transaksi transaksi = getTransaksi();
        transaksiRepository.save(transaksi);
        List<Transaksi> transaksiList = transaksiRepository.findAll();
        Assertions.assertNotEquals(4, transaksiList.size());
        transaksiRepository.delete(transaksiRepository.save(transaksi));
    }

//    @Test
//    public void testUpdateTransaksiPositiveCase(){
//        Transaksi transaksi = getTransaksi();
//        transaksiRepository.save(transaksi);
//
//        transaksi.setId(1);
//        transaksi.setKodeTransaksi("TR667788");
//        transaksiRepository.save(transaksi);
//
//        Optional<Transaksi> t = transaksiRepository.findById(transaksiRepository.save(transaksiRepository.save(transaksi)).getId());
//        Assertions.assertTrue(t.isPresent());
//        Assertions.assertEquals(1, t.get().getId());
//        Assertions.assertEquals("TR667788", t.get().getKodeTransaksi());
//        transaksiRepository.delete(transaksiRepository.save(transaksi));
//    }

    @Test
    public void testUpdateTransaksiNegativeCase(){
        Transaksi transaksi = getTransaksi();
        transaksiRepository.save(transaksi);

        transaksi.setId(1);
        transaksi.setKodeTransaksi("TR667788");
        transaksiRepository.save(transaksi);

        Optional<Transaksi> t = transaksiRepository.findById(transaksiRepository.save(transaksiRepository.save(transaksi)).getId());
        Assertions.assertTrue(t.isPresent());
        Assertions.assertNotEquals(2, t.get().getId());
        Assertions.assertNotEquals("TR112233", t.get().getKodeTransaksi());
        transaksiRepository.delete(transaksiRepository.save(transaksi));
    }

    @Test
    public void testDeleteTransaksiPositiveCase(){
        Transaksi transaksi = getTransaksi();
        transaksiRepository.save(transaksi);

        transaksiRepository.delete(transaksi);
        Optional<Transaksi> t = transaksiRepository.findById(transaksi.getId());

        Assertions.assertFalse(t.isPresent());
        transaksiRepository.delete(transaksiRepository.save(transaksi));
    }

    @Test
    public void testDeleteTransaksiNegativeCase(){
        Transaksi transaksi = getTransaksi();

        transaksiRepository.delete(transaksi);
        Optional<Transaksi> t = transaksiRepository.findById(transaksi.getId());

        Assertions.assertFalse(t.isPresent());
    }
}
