package com.PointOfSales.repository;

import com.PointOfSales.PointOfSalesApplication;
import com.PointOfSales.entity.Karyawan;
import com.PointOfSales.entity.Login;
import com.PointOfSales.util.JwtToken;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@SpringBootTest(classes = PointOfSalesApplication.class)
public class KaryawanRepositoryTest {

    @Autowired
    private KaryawanRepository karyawanRepository;

    @Autowired
    private LoginRepository loginRepository;


    private Karyawan getKaryawan(){
        Karyawan karyawan = new Karyawan();
        karyawan.setId(1);
        karyawan.setNik("32022948");
        karyawan.setNama("Syifa Nabila");
        karyawan.setAlamat("Sukabumi");
        karyawan.setJenisKelamin("Perempuan");
        return karyawan;
    }

    private Login getLogin(){
        Login login = new Login();
        login.setEmail("syifa@gmail.com");
        login.setPassword("12345");
        login.setTanggalLogin(LocalDate.now());
        login.setRole("Karyawab");
        login.setToken("qwertyuiop");
        return login;
    }


//    @Test
//    public void testSaveKaryawanPositiveCase(){
//        Karyawan karyawan = getKaryawan();
//        Login login = getLogin();
//        karyawanRepository.save(karyawan);
//        loginRepository.save(login);
//        Optional<Karyawan> k = karyawanRepository.findById(karyawanRepository.save(karyawanRepository.save(karyawan)).getId());
//        Optional<Login> l = loginRepository.findByEmailAndPassword(login.getEmail(), login.getPassword());
//        Assertions.assertEquals(karyawan.getId(), k.get().getId());
//        Assertions.assertEquals(karyawan.getNik(), k.get().getNik());
//        Assertions.assertEquals(karyawan.getNama(), k.get().getNama());
//        Assertions.assertEquals(karyawan.getAlamat(), k.get().getAlamat());

//        Assertions.assertEquals("1", k.get().getId().toString());
//        Assertions.assertEquals(login.getEmail(), l.get().getEmail());
//        Assertions.assertEquals(login.getPassword(), l.get().getPassword());
//        Assertions.assertEquals(login.getTanggalLogin(), l.get().getTanggalLogin());
//        Assertions.assertEquals(login.getRole(), l.get().getRole());
//        Assertions.assertEquals(login.getToken(), l.get().getToken());

//        karyawanRepository.delete(karyawanRepository.save(karyawan));
////        loginRepository.delete(loginRepository.save(login));
//    }

    @Test
    public void testSaveKaryawanNegativeCase(){
        Karyawan karyawan = getKaryawan();
//        Login login = getLogin();
        karyawanRepository.save(karyawan);
//        loginRepository.save(login);
        Optional<Karyawan> k = karyawanRepository.findById(karyawanRepository.save(karyawanRepository.save(karyawan)).getId());
//        Optional<Login> l = loginRepository.findByEmailAndPassword(login.getEmail(), login.getPassword());
//
        Assertions.assertNotEquals(2, k.get().getId());
        Assertions.assertNotEquals("3209878", k.get().getNik());
        Assertions.assertNotEquals("Kadi", k.get().getNama());
        Assertions.assertNotEquals("Jakarata", k.get().getAlamat());
        Assertions.assertNotEquals("Wanita", k.get().getJenisKelamin());

//        Assertions.assertNotEquals("nabila@gamil.com", l.get().getEmail());
//        Assertions.assertNotEquals("akubisa", l.get().getPassword());
//        Assertions.assertNotEquals(LocalDate.now().minusDays(2), l.get().getTanggalLogin());
//        Assertions.assertNotEquals("mahasiswa", l.get().getRole());
//        Assertions.assertNotEquals("bismillah", l.get().getToken());

        karyawanRepository.delete(karyawanRepository.save(karyawan));
//        loginRepository.delete(loginRepository.save(login));
    }

//    @Test
//    public void testFindAllKaryawanPositiveCase(){
//        Karyawan karyawan = getKaryawan();
//        karyawanRepository.save(karyawan);
//        Login login = getLogin();
//        loginRepository.save(login);
//        List<Karyawan> karyawanList = karyawanRepository.findAll();
//        List<Login> loginList = loginRepository.findAll();
//        Assertions.assertEquals(1, karyawanList.size());
//        Assertions.assertEquals(1, loginList.size());
//        karyawanRepository.delete(karyawanRepository.save(karyawan));
//        loginRepository.delete(loginRepository.save(login));
//    }

    @Test
    public void testFindAllKaryawanNegativeCase(){
        Karyawan karyawan = getKaryawan();
        karyawanRepository.save(karyawan);
        Login login = getLogin();
        loginRepository.save(login);
        List<Karyawan> karyawanList = karyawanRepository.findAll();
        List<Login> loginList = loginRepository.findAll();
        Assertions.assertNotEquals(4, karyawanList.size());
        Assertions.assertNotEquals(4, loginList.size());
        karyawanRepository.delete(karyawanRepository.save(karyawan));
        loginRepository.delete(loginRepository.save(login));
    }

    @Test
    public void testUpdateKaryawanPositiveCase(){
        Karyawan karyawan = getKaryawan();
        karyawanRepository.save(karyawan);
        Login login = getLogin();
        loginRepository.save(login);

        karyawan.setNama("Syifa Nabila Kadi");
        karyawan.setAlamat("Kelapa Gading");
        login.setEmail("nabila1234@gmail.com");
        login.setPassword("qwerty");
        karyawanRepository.save(karyawan);
        loginRepository.save(login);

        Optional<Karyawan> k = karyawanRepository.findById(karyawanRepository.save(karyawanRepository.save(karyawan)).getId());
        Optional<Login> l = loginRepository.findByEmailAndPassword(loginRepository.save(loginRepository.save(login)).getEmail(),
                loginRepository.save(loginRepository.save(login)).getPassword());
        Assertions.assertTrue(k.isPresent());
        Assertions.assertTrue(l.isPresent());
        Assertions.assertEquals("Syifa Nabila Kadi", k.get().getNama());
        Assertions.assertEquals("Kelapa Gading", k.get().getAlamat());
        Assertions.assertEquals("nabila1234@gmail.com", l.get().getEmail());
        Assertions.assertEquals("qwerty", l.get().getPassword());
        karyawanRepository.delete(karyawanRepository.save(karyawan));
        loginRepository.delete(loginRepository.save(login));
    }

    @Test
    public void testUpdateKaryawanNegativeCase(){
        Karyawan karyawan = getKaryawan();
        karyawanRepository.save(karyawan);
        Login login = getLogin();
        loginRepository.save(login);

        karyawan.setNama("Syifa Nabila Kadi");
        karyawan.setAlamat("Kelapa Gading");
        login.setEmail("nabila1234@gmail.com");
        login.setPassword("qwerty");
        karyawanRepository.save(karyawan);
        loginRepository.save(login);

        Optional<Karyawan> k = karyawanRepository.findById(karyawanRepository.save(karyawanRepository.save(karyawan)).getId());
        Assertions.assertTrue(k.isPresent());
        Optional<Login> l = loginRepository.findByEmailAndPassword(loginRepository.save(loginRepository.save(login)).getEmail(),
                loginRepository.save(loginRepository.save(login)).getPassword());
        Assertions.assertNotEquals("FIlza Syifa", k.get().getNama());
        Assertions.assertNotEquals("Bandung", k.get().getAlamat());
        Assertions.assertNotEquals("nnnn1234@gmail.com", l.get().getEmail());
        Assertions.assertNotEquals("qwertyuiop", l.get().getPassword());
        karyawanRepository.delete(karyawanRepository.save(karyawan));
        loginRepository.delete(loginRepository.save(login));
    }

    @Test
    public void testDeleteKaryawanPositiveCase(){
        Karyawan karyawan = getKaryawan();
        karyawanRepository.save(karyawan);
        Login login = getLogin();
        loginRepository.save(login);

        loginRepository.delete(login);
        Optional<Login> l = loginRepository.findByEmailAndPassword(login.getEmail(), login.getPassword());

        karyawanRepository.delete(karyawan);
        Optional<Karyawan> k = karyawanRepository.findById(karyawan.getId());

        Assertions.assertFalse(k.isPresent());
        Assertions.assertFalse(l.isPresent());
        loginRepository.delete(loginRepository.save(login));
        karyawanRepository.delete(karyawanRepository.save(karyawan));
    }

    @Test
    public void testDeleteKaryawanNegativeCase(){
        Karyawan karyawan = getKaryawan();
        Login login = getLogin();

        karyawanRepository.delete(karyawan);
        Optional<Karyawan> k = karyawanRepository.findById(karyawan.getId());

        loginRepository.delete(login);
        Optional<Login> l = loginRepository.findByEmailAndPassword(login.getEmail(), login.getPassword());

        Assertions.assertFalse(l.isPresent());
        Assertions.assertFalse(k.isPresent());
    }
}
