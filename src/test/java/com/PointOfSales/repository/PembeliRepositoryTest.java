package com.PointOfSales.repository;

import com.PointOfSales.PointOfSalesApplication;
import com.PointOfSales.entity.Pembeli;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;

@SpringBootTest(classes = PointOfSalesApplication.class)
public class PembeliRepositoryTest {

    @Autowired
    private PembeliRepository pembeliRepository;

    private Pembeli getPembeli(){
        Pembeli pembeli = new Pembeli();
        pembeli.setId(1);
        pembeli.setNamaPembeli("Syifa Nabila");
        pembeli.setAlamat("Sulabumi");
        pembeli.setNoTelepon("085219717446");
        return pembeli;
    }

    @Test
    public void testSavePembeliPositiveCase(){
        Pembeli pembeli = getPembeli();
        pembeliRepository.save(pembeli);
        Optional<Pembeli> p = pembeliRepository.findById(pembeliRepository.save(pembeliRepository.save(pembeli)).getId());
        Assertions.assertEquals(pembeli.getId(), p.get().getId());
        Assertions.assertEquals(pembeli.getNamaPembeli(), p.get().getNamaPembeli());
        Assertions.assertEquals(pembeli.getAlamat(), p.get().getAlamat());
        Assertions.assertEquals(pembeli.getNoTelepon(), p.get().getNoTelepon());
        pembeliRepository.delete(pembeliRepository.save(pembeli));
    }

    @Test
    public void testSavePembeliNegativeCase(){
        Pembeli pembeli = getPembeli();
        pembeliRepository.save(pembeli);
        Optional<Pembeli> p = pembeliRepository.findById(pembeliRepository.save(pembeliRepository.save(pembeli)).getId());
        Assertions.assertNotEquals(2, p.get().getId());
        Assertions.assertNotEquals("Endah Nugrahini", p.get().getNamaPembeli());
        Assertions.assertNotEquals("Pandeglang", p.get().getAlamat());
        Assertions.assertNotEquals("08524567677", p.get().getNoTelepon());
        pembeliRepository.delete(pembeliRepository.save(pembeli));
    }

//    @Test
//    public void testFindAllPembeliPositiveCase(){
//        Pembeli pembeli = getPembeli();
//        pembeliRepository.save(pembeli);
//        List<Pembeli> pembeliList = pembeliRepository.findAll();
//        Assertions.assertEquals(1, pembeliList.size());
//        pembeliRepository.delete(pembeliRepository.save(pembeli));
//    }

    @Test
    public void testFindAllPembeliNegativeCase(){
        Pembeli pembeli = getPembeli();
        pembeliRepository.save(pembeli);
        List<Pembeli> pembeliList = pembeliRepository.findAll();
        Assertions.assertNotEquals(4, pembeliList.size());
        pembeliRepository.delete(pembeliRepository.save(pembeli));
    }

    @Test
    public void testUpdatePembeliPositiveCase(){
        Pembeli pembeli = getPembeli();
        pembeliRepository.save(pembeli);

        pembeli.setNamaPembeli("Syifa Nabila Kadi");
        pembeli.setAlamat("Kelapa Gading");
        pembeli.setNoTelepon("085860671625");
        pembeliRepository.save(pembeli);

        Optional<Pembeli> p = pembeliRepository.findById(pembeliRepository.save(pembeliRepository.save(pembeli)).getId());
        Assertions.assertTrue(p.isPresent());
        Assertions.assertEquals("Syifa Nabila Kadi", p.get().getNamaPembeli());
        Assertions.assertEquals("Kelapa Gading", p.get().getAlamat());
        Assertions.assertEquals("085860671625", p.get().getNoTelepon());
        pembeliRepository.delete(pembeliRepository.save(pembeli));
    }

    @Test
    public void testUpdatePembeliNegativeCase(){
        Pembeli pembeli = getPembeli();
        pembeliRepository.save(pembeli);

        pembeli.setNamaPembeli("Syifa Nabila Kadi");
        pembeli.setAlamat("Kelapa Gading");
        pembeli.setNoTelepon("085860671625");
        pembeliRepository.save(pembeli);

        Optional<Pembeli> p = pembeliRepository.findById(pembeliRepository.save(pembeliRepository.save(pembeli)).getId());
        Assertions.assertTrue(p.isPresent());
        Assertions.assertNotEquals("FIlza Syifa", p.get().getNamaPembeli());
        Assertions.assertNotEquals("Bandung", p.get().getAlamat());
        Assertions.assertNotEquals("09876789008", p.get().getAlamat());
        pembeliRepository.delete(pembeliRepository.save(pembeli));
    }

    @Test
    public void testDeletePembeliPositiveCase(){
        Pembeli pembeli = getPembeli();
        pembeliRepository.save(pembeli);

        pembeliRepository.delete(pembeli);
        Optional<Pembeli> p = pembeliRepository.findById(pembeli.getId());

        Assertions.assertFalse(p.isPresent());
        pembeliRepository.delete(pembeliRepository.save(pembeli));
    }

    @Test
    public void testDeletePembeliNegativeCase(){
        Pembeli pembeli = getPembeli();

        pembeliRepository.delete(pembeli);
        Optional<Pembeli> p = pembeliRepository.findById(pembeli.getId());

        Assertions.assertFalse(p.isPresent());
    }
}
